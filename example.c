#include <gtk/gtk.h>

#include <stdio.h>
#include "include/c/sk_canvas.h"
#include "include/c/sk_data.h"
#include "include/c/sk_image.h"
#include "include/c/sk_paint.h"
#include "include/c/sk_path.h"
#include "include/c/sk_surface.h"
#include "include/c/gr_context.h"

#include "sk_area.h"

// --- skia

void draw(SKArea *sk_area, sk_canvas_t *canvas, gpointer data)
{
	static int frame_number = 0;
	sk_paint_t* fill = sk_paint_new();
	sk_paint_set_color(fill, sk_color_set_argb(0xFF, 0x00, 0x00, 0xFF));
	sk_canvas_draw_paint(canvas, fill);
	sk_paint_set_color(fill, sk_color_set_argb(0xFF, 0x00, 0xFF, 0xFF));
	sk_rect_t rect;
	rect.left = 100.0f;
	rect.top = 100.0f;
	rect.right = 540.0f;
	rect.bottom = 380.0f;
	sk_canvas_draw_rect(canvas, &rect, fill);
	sk_paint_t* stroke = sk_paint_new();
	sk_paint_set_color(stroke, sk_color_set_argb(0xFF, 0xFF, 0x00, 0x00));
	sk_paint_set_antialias(stroke, true);
	sk_paint_set_style(stroke, STROKE_SK_PAINT_STYLE);
	sk_paint_set_stroke_width(stroke, 5.0f);
	sk_path_t* path = sk_path_new();
	sk_path_move_to(path, 50.0f, 50.0f);
	sk_path_line_to(path, 590.0f, 50.0f);
	sk_path_cubic_to(path, -490.0f, 50.0f, 1130.0f, 430.0f, 50.0f, 430.0f); // this causes the path to not render properly with antialiasing on
	sk_path_line_to(path, 590.0f, 430.0f);
	sk_canvas_draw_path(canvas, path, stroke);
	sk_canvas_draw_line(canvas, 0, 0, 100, 100, stroke);
	sk_paint_set_color(fill, sk_color_set_argb(0x80, 0x00, 0xFF, frame_number));
	sk_rect_t rect2;
	rect2.left = 120.0f;
	rect2.top = 120.0f;
	rect2.right = 520.0f;
	rect2.bottom = 360.0f;
	sk_canvas_draw_oval(canvas, &rect2, fill);
	sk_path_delete(path);
	sk_paint_delete(stroke);
	sk_paint_delete(fill);

	frame_number++;
	if(frame_number > 0xFF)
		frame_number = 0;
}

// --- gtk

gboolean tick_callback(GtkWidget* widget, GdkFrameClock* frame_clock, gpointer user_data)
{
	gtk_widget_queue_draw(widget);
	return G_SOURCE_CONTINUE;
}

static void button_clicked(GtkButton *button, GtkWindow *window)
{
	GtkWidget *child = gtk_window_get_child(window);
	g_object_ref(G_OBJECT(child));
	printf("child: %p\n", child);
	gtk_window_set_child(window, NULL);
	gtk_window_set_child(window, child);
}

static void activate(GtkApplication *app, gpointer user_data)
{
	GtkWidget *window;
	GtkWidget *sk_area;
	window = gtk_application_window_new(app);
	gtk_window_set_title(GTK_WINDOW(window), "GL Area");
	sk_area = sk_area_new();
	sk_area_set_draw_func(SK_AREA_WIDGET(sk_area), draw, NULL);
	/* set a minimum size */
	gtk_widget_set_size_request(sk_area, 640, 400);
	gtk_window_set_child(GTK_WINDOW(window), sk_area);
	gtk_widget_add_tick_callback(sk_area, tick_callback, NULL, NULL);

	GtkWidget *header_bar = gtk_header_bar_new();
	GtkWidget *button = gtk_button_new();
	gtk_button_set_label(GTK_BUTTON(button), "unplug/replug");
	g_signal_connect(button, "clicked", G_CALLBACK(button_clicked), window);
	gtk_header_bar_pack_start(GTK_HEADER_BAR(header_bar), button);
	gtk_window_set_titlebar(GTK_WINDOW(window), header_bar);

	gtk_window_present(GTK_WINDOW(window));
}

int main(int argc, char **argv)
{
	GtkApplication *app;
	int status;
	app = gtk_application_new("org.gtk.example", G_APPLICATION_DEFAULT_FLAGS);
	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
	status = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);
	return status;
}
