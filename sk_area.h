#ifndef SK_AREA_H
#define SK_AREA_H

#include "include/c/sk_canvas.h"

G_DECLARE_FINAL_TYPE(SKArea, sk_area, SK_AREA, WIDGET, GtkGLArea)

GtkWidget * sk_area_new(void);
void sk_area_set_draw_func(SKArea *sk_area, void (*draw_func)(SKArea *sk_area, sk_canvas_t *canvas, gpointer data), gpointer data);

#endif
