### SKArea widget for Gtk4 using the C API provided by the SkiaSharp fork of Skia

to compile an example: `make`  
to run using the bundled skia .so: `LD_LIBRARY_PATH=. ./sk-area-example`  

ideally, the C API provided by SkiaSharp will be made stable, such that both the includes
and the library could be packaged by distros (see https://github.com/mono/SkiaSharp/discussions/2585)

skiasharp repo: https://github.com/mono/SkiaSharp  
the actual skia fork: https://github.com/mono/skia  

